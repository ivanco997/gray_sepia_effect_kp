import java.awt.Color;
import java.awt.image.BufferedImage;
import java.awt.image.ColorConvertOp;
import java.io.File;
import java.io.IOException;
import java.awt.EventQueue;
import java.awt.color.ColorSpace;
import javax.imageio.ImageIO;


public class SepiaGrayImage {
	
	private static final String ORIGINAL_IMAGE_PATH = "./dog.jpg";
	private static final String SOURCE_IMAGE_PATH = "./source.jpg";
	private static final String IMAGE_FORMAT_NAME = "jpg";

	public static void main(String[] args) {
	    BufferedImage originalImage = null;
	    File f = null;

	    try {
	      f = new File(ORIGINAL_IMAGE_PATH);
	      originalImage = ImageIO.read(f);
	    } catch(IOException e) {
	      e.printStackTrace();
	    }

	    BufferedImage newImage = imageSepiaEffect(originalImage, 10);
	    BufferedImage grayImage = imageGrayScaleEffect(originalImage);

	    try {
	      f = new File(SOURCE_IMAGE_PATH);
	      ImageIO.write(newImage, IMAGE_FORMAT_NAME, f);
	    } catch(IOException e) {
	      e.printStackTrace();
	    }
	   
	    EventQueue.invokeLater(new PanelRunnable(newImage, grayImage, originalImage));
	}
	
	private static BufferedImage imageSepiaEffect(BufferedImage img, int sepiaIntensity) {

	    BufferedImage sepia = new BufferedImage(img.getWidth(), img.getHeight(), BufferedImage.TYPE_INT_ARGB);

	    int sepiaDepth = 20;

	    int w = img.getWidth();
	    int h = img.getHeight();

	    /* Трябват ни 3 числа които представяшат трите основни цшята (RGB - RED, GREEN, BLUE)
	     за всеки пиксел */

	    int[] pixels = new int[w * h * 3];
	    img.getRaster().getPixels(0, 0, w, h, pixels);

	    for (int x = 0; x < img.getWidth(); x++) {
	        for (int y = 0; y < img.getHeight(); y++) {

	            int rgb = img.getRGB(x, y);
	            Color color = new Color(rgb, true);
	            int r = color.getRed();
	            int g = color.getGreen();
	            int b = color.getBlue();
	            int gry = (r + g + b) / 3;

	            r = g = b = gry;
	            r = r + (sepiaDepth * 2);
	            g = g + sepiaDepth;

	            if (r > 255) {
	                r = 255;
	            }
	            if (g > 255) {
	                g = 255;
	            }
	            if (b > 255) {
	                b = 255;
	            }

	            b -= sepiaIntensity;

	            if (b < 0) {
	                b = 0;
	            }
	            if (b > 255) {
	                b = 255;
	            }

	            color = new Color(r, g, b, color.getAlpha());
	            sepia.setRGB(x, y, color.getRGB());
	        }
	    }
	    return sepia;
	}
	
    public static BufferedImage imageGrayScaleEffect(BufferedImage master) {
        BufferedImage gray = new BufferedImage(master.getWidth(), master.getHeight(), BufferedImage.TYPE_INT_ARGB);

        // Автоматично конвертиране
        ColorConvertOp op = new ColorConvertOp(ColorSpace.getInstance(ColorSpace.CS_GRAY), null);
        op.filter(master, gray);

        return gray;
    }
}
