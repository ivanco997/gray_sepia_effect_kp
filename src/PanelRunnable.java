import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

public class PanelRunnable implements Runnable {
	
	private BufferedImage sepiaImage;
	private BufferedImage grayImage;
	private BufferedImage originalImage;
	
	private static final String SEPIA_BUTTON = "Show sepia effect";
	private static final String GRAY_BUTTON = "Show gray effect";
	private static final String ORIGINAL_BUTTON = "Show original image";
	
	private static final String FIRST_PANEL = "first";
	private static final String SECOND_PANEL = "second";
	private static final String THIRD_PANEL = "third";
	
	private static final String WINDOW_TITLE = "Gray and sepia image effect";
	
	public PanelRunnable(BufferedImage sepiaImage, BufferedImage grayImage, BufferedImage originalImage) {
		this.sepiaImage = sepiaImage;
		this.grayImage = grayImage;
		this.originalImage = originalImage;
	}
	
	  @Override
	    public void run() {
	        JFrame frm = new JFrame(WINDOW_TITLE);
	        frm.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
	        JPanel contentPanel = new JPanel(new BorderLayout());
	        frm.setContentPane(contentPanel);
	        
	        JPanel mainPanel = new JPanel(new CardLayout());
	        mainPanel.add(new JLabel(new ImageIcon(this.grayImage)), FIRST_PANEL);
	        mainPanel.add(new JLabel(new ImageIcon(this.sepiaImage)), SECOND_PANEL);
	        mainPanel.add(new JLabel(new ImageIcon(this.originalImage)), THIRD_PANEL);
	        mainPanel.add(Box.createHorizontalStrut(600));
	        mainPanel.add(Box.createVerticalStrut(300));
	        mainPanel.setBackground(Color.CYAN);
	        
	        JPanel settingsPanel = new JPanel(new GridLayout(1, 1));
	        settingsPanel.add(new JLabel(new ImageIcon(this.sepiaImage)));
	        settingsPanel.setPreferredSize(
	                new Dimension(settingsPanel.getPreferredSize().width, 300));
	        ((JComponent) frm.getGlassPane()).setLayout(new FlowLayout(FlowLayout.RIGHT, 0, 0));
	        ((JComponent) frm.getGlassPane()).add(settingsPanel, BorderLayout.EAST);
	        
	        JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT, 10, 10));
	        
	        JButton settingsButton = new JButton(ORIGINAL_BUTTON);
	        settingsButton.addActionListener(new ActionListener() {

	            @Override
	            public void actionPerformed(ActionEvent e) {
	                CardLayout cl = (CardLayout) mainPanel.getLayout();
	            	cl.show(mainPanel, THIRD_PANEL);
	            }
	        });

	        JButton switchButton = new JButton(SEPIA_BUTTON);
	        switchButton.addActionListener(new ActionListener() {

	            @Override
	            public void actionPerformed(ActionEvent e) {
	                CardLayout cl = (CardLayout) mainPanel.getLayout();
	                if (mainPanel.getComponent(0).isVisible()) {
	                    cl.show(mainPanel, SECOND_PANEL);
	                    switchButton.setText(GRAY_BUTTON);
	                } else {
	                    cl.show(mainPanel, FIRST_PANEL);
	                    switchButton.setText(SEPIA_BUTTON);
	                }
	            }
	        });
	        
	        buttonPanel.add(switchButton);
	        buttonPanel.add(settingsButton);
	        frm.add(mainPanel, BorderLayout.CENTER);
	        frm.add(buttonPanel, BorderLayout.SOUTH);
	        frm.pack();
	        frm.setLocationRelativeTo(null);
	        frm.setVisible(true);
	  }
}
